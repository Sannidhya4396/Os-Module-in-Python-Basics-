import os

# print(dir(os)) Prints the functions that can be performed
# print(os.getcwd()) Prints the directory path
# os.chdir("C://") This changes the current working directory 
# print(os.listdir("C://")) Prints the list of items in the given directory
# os.mkdir("Test Folder") Create a new Folder with the name you wish
# os.makedirs("Python/Codes") # Create a new directory
# os.rename("test.txt", "test_file.txt") Rename teh fiel in the first parameter with the name in the second parameter 
# print(os.environ.get('PATH')) Prints the path of Envoirment Variables 
# print(os.path.join("C://", "test_file.txt") Joins the first patth with the second
print(os.path.exists("C://")) # Prints if this path exists or not
